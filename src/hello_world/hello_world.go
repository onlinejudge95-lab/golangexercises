package helloworld

import "fmt"

func HelloWorld() {
	// Prints hello world
	fmt.Println("Hello, world!!")
}

func PrintName() {
	/* This function prints the name
	   It can be imported into any namespace */
	fmt.Println("My name is Mayank Pathak!!")
}

func PrintAddress() {
	fmt.Println("My address is DUMMY ADDRESS!!")
}
